#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QPainter>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}



void MainWindow::mousePressEvent(QMouseEvent *event)
{


    if (count==0)
    {
         x_1 = event->x();
         y_1 = event->y();
        //qDebug(" x=%d y=%d",x_1,y_1);
        count++;

    }
    else if (count==1)
    {

        r=Rect(x_1,y_1,event->x(), event->y());
        //qDebug(" x=%d y=%d",event->x(),event->y());
        count++;

    }

    else if (count==2)
    {
        x_1 = event->x();
        y_1 = event->y();
        count++;
    }

    else if(count==3)
    {
        r_2 = Rect(x_1,y_1,event->x(),event->y());
        count++;

    }

    //count++;

    if (count>3)
    {
       r_3=r.intersection(r_2);

    }


    repaint();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    if(count>=2)
    {
        //qDebug(" heigth=%d width=%d",r.getHeigth(),r.getWidth());
        r.draw(&painter,1);


    }
    if(count >=3)
    {
        r_2.draw(&painter,2);
        r_3.draw(&painter,3);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

