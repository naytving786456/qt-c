#include "rect.h"
using namespace std;

Rect::Rect()
{

}

Rect::Rect(int x1, int y1, int x2, int y2)
{
    //если x1(y1) меньше x2(y2) то x1(y1)=x1(y1), иначе x1(y1)=x2(y2) и наоборот
    this->x1=x1 < x2 ? x1:x2;
    this->y1=y1 < y2 ? y1:y2;
    this->x2=x2 > x1 ? x2:x1;
    this->y2=y2 > y1 ? y2:y1;

}

bool Rect::cont(int x, int y)
{
    if (x<x1) return false;
    if (x>x2) return false;
    if (y<y1) return false;
    if (y>y2) return false;

    return true;
}

//int Rect::getX_1() const
//{
//    return x1;
//}

//void Rect::setX_1(int value)
//{
//    x1 = value;
//}

//int Rect::getY_1()
//{
//    return y1;
//}

//void Rect::setY_1(int value)
//{
//    y1 = value;
//}

//int Rect::getX_2() const
//{
//    return x2;
//}

//void Rect::setX_2(int value)
//{
//    x2=value;
//}

//int Rect::getY_2()
//{
//    return y2;
//}

//void Rect::setY_2(int value)
//{
//    y2 = value;
//}

void Rect::draw(QPainter *painter,int num)
{
    if(num==1)
    {
        painter->setBrush(Qt::green);
    }
    else if(num==2)
    {
        painter->setBrush(Qt::blue);
    }
    else if(num==3)
    {
        painter->setBrush(Qt::white);
    }

    painter->drawRect(x1,y1,getWidth(),getHeigth());

}

Rect Rect::intersection(Rect r)
{
    int n_x1, n_y1,n_x2,n_y2;

    if (r.x1>this->x2 || r.x2<this->x1 || r.y1>this->y2 || r.y2<this->y1)
        n_x1=n_y1=n_x2=n_y2=0;

    else
    {
        n_x1 = max(r.x1, x1);
        n_y1 = max(r.y1,y1);
        n_x2 = min(r.x2,x2);
        n_y2 = min(r.y2,y2);
    }
   Rect r_3=Rect(n_x1,n_y1,n_x2,n_y2);
   qDebug("square=%d",r_3.getSquare());
   return r_3;
}

