#ifndef RECT_H
#define RECT_H
#include <QPainter>
#include <QPen>

class QPainter;
class Rect
{
public:
    Rect();
    Rect(int x1, int y1, int x2, int y2);

    bool cont(int x, int y);// проверка на прямоугольник


//    int getX_1() const;
//    void setX_1(int value);

//    int getY_1();
//    void setY_1(int value);

//    int getX_2() const;
//    void setX_2(int value);

//    int getY_2();
//    void setY_2(int value);

    void draw(QPainter *painter,int num);

    Rect intersection(Rect r);


    int getWidth() {return x2-x1;}//вычисление ширины
    int getHeigth() {return y2-y1;} //вычисление высоты
    int getSquare() {return getWidth()*getHeigth();}//вычисление площади


private:
    int x1,y1,x2,y2;
};

#endif // RECT_H
