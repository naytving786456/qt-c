#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QPainter>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (count==0)
    {

        r.setX_1(event->x());
        r.setY_1(event->y());
        //qDebug(" x=%d y=%d",r.getX_1(),r.getY_1());
        //count++;

    }
    if (count==1)
    {

        r.setX_2(event->x());
        r.setY_2(event->y());
        r=Rect(r.getX_1(),r.getY_1(),r.getX_2(),r.getY_2());
        //qDebug(" x=%d y=%d",r.getX_2(),r.getY_2());
        //count++;
    }

    if(count>=2)
    {

        if(r.cont(event->x(),event->y()))
        {
              //qDebug(" x=%d y=%d",event->x(),event->y());
            QMessageBox msgBox;
            msgBox.setWindowTitle("Сообщение");
            msgBox.setText("Попал");
            msgBox.exec();

        }
        else
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Сообщение");
            msgBox.setText("Промазал");
            msgBox.exec();
        }
    }

    count++;

    repaint();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    if(count>=2)
    {
        //qDebug(" x=%d y=%d",r.getX_2(),r.getY_2());
        painter.drawRect(r.getX_1(),r.getY_1(),r.getWidth(),r.getHeigth());

    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

