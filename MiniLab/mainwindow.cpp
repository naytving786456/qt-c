#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "rect.h"
#include <QPainter>
#include <QMouseEvent>
#include <QMessageBox>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    Rect *r = nullptr;
    if(event->modifiers() & Qt::ControlModifier) {
        if(n % 2 == 0) {
            cX = event->x();
            cY = event->y();
        }
        else {
            r = new Rect(cX, cY, event->x(), event->y());
            box.append(r);
        }
        n++;
    }
    else {
        int x1 = 0, y1 = 0, x2 = 100000, y2 = 100000, inter = 0; bool bom = false;
        for(int i = 0; i < box.size(); i++)
            if(box[i]->contain(event->x(), event->y())) {
                if((box[i]->getX1() != x1) || (box[i]->getX2() != x2) || (box[i]->getY1() != y1) || (box[i]->getY2() != y2)) {
                    if(x1 < box[i]->getX1()) x1 = box[i]->getX1();
                    if(y1 < box[i]->getY1()) y1 = box[i]->getY1();
                    if(x2 > box[i]->getX2()) x2 = box[i]->getX2();
                    if(y2 > box[i]->getY2()) y2 = box[i]->getY2();
                    if(box[i]->getCont() <= 1) {
                        inter++;
                        bom = false;
                    }
                }
                else bom = true;
            }
        if((!bom) && (inter > 1)) {
            r = new Rect(x1, y1, x2, y2, inter);
            box.append(r);
        }
    }
    repaint();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    for(int j = 2; j < box.size(); j++)
        for(int i = 0; i < box.size(); i++)
            if(box[i]->getCont() == j)
                box[i]->draw(&painter);
    for(int i = 0; i < box.size(); i++)
        if(box[i]->getCont() <= 1)
            box[i]->draw(&painter);
}
