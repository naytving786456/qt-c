#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    for(int i=0;i<10;i++)
        p[i]=nullptr;
}


void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    for (int i=0; i<10;i++)
    {
        if(p[i])
        {
            p[i]->draw(&painter);
        }
    }

    QPen pen_lines;
    painter.setPen(pen_lines);
    for(int i=0;i<count-1;i++)
    {
        painter.drawLine(p[i]->getX(),p[i]->getY(),p[i+1]->getX(),p[i+1]->getY());
    }

}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if(count<10)
    {
        if(count==0)
        {
            p[count] = new pointer(event->x(), event->y(),3);
        }

        else
        {
            p[count] = new pointer(event->x(), event->y(),p[count-1]->getRaz()+1);
        }

        if(count>=0 && count<=4)
        {
            p[count]->setWidth(1);
        }

        else
        {
            p[count]->setWidth(2);
        }

        count++;
    }

    repaint();
}

void MainWindow::Update()
{
    for(int i=0;i<count;i++)
    {
        if(!p[i])
        {
            return;
        }
    }
    repaint();
}

MainWindow::~MainWindow()
{
    delete ui;
}


