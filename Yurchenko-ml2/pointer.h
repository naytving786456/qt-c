#ifndef POINTER_H
#define POINTER_H
#include <QPainter>
#include <Qpen>

class QPainter;
class pointer
{
public:
    pointer();
    pointer(int x, int y, int raz);

    int getX() const;
    void setX(int value);

    int getY() const;
    void setY(int value);

    void draw(QPainter *painter);

    int getRaz() const;
    void setRaz(int value);

    int getWidth() const;
    void setWidth(int value);

private:
    int x, y, raz, width;
};

#endif // POINTER_H
