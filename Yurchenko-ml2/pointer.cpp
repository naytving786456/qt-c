#include "pointer.h"

pointer::pointer()
{

}

pointer::pointer(int x, int y, int raz)
    :x(x), y(y), raz(raz)
{

}



int pointer::getX() const
{
    return x;
}

void pointer::setX(int value)
{
    x = value;
}

int pointer::getY() const
{
    return y;
}

void pointer::setY(int value)
{
    y = value;
}

void pointer::draw(QPainter *painter)
{
    QPen pen;
    pen.setWidth(width);
    painter->setPen(pen);
    painter->drawLine(x-raz, y, x+raz, y);
    painter->drawLine(x,y-raz,x,y+raz);

}

int pointer::getRaz() const
{
    return raz;
}

void pointer::setRaz(int value)
{
    raz=value;
}

int pointer::getWidth() const
{
    return width;
}

void pointer::setWidth(int value)
{
    width = value;
}
