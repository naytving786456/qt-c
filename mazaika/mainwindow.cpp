#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QPainter>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}



void MainWindow::mousePressEvent(QMouseEvent *event)
{
    Rect r;
    Rect r_tmp;

    if (event->modifiers() & Qt::ControlModifier)
    {
        if(num%2==0)
        {
            x_1 = event->x();
            y_1 = event->y();
        }

        else
        {
            r = Rect(x_1,y_1,event->x(),event->y());
            rect.append(r);
        }
        num++;
    }

    else
    {   int intr = 0 ;
        for(int i=0; i<rect.size();i++)
        {
            if(rect[i].cont(event->x(),event->y()))
            {
                if(intr == 0 )
                    r_tmp = Rect(rect[i].getX_1(),rect[i].getY_1(),rect[i].getX_2(),rect[i].getY_2());
                else
                    r_tmp = r_tmp.intersection(rect[i]);

                intr++;
            }

         }
        if (intr>1)
        {
            r_tmp.setInt(intr);
            rect_int.append(r_tmp);
        }
    }
    repaint();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    for(int j = 2; j < 6; j++)
        for(int i = 0; i < rect_int.size(); i++)
            if(rect_int[i].getInt() == j)
                rect_int[i].draw(&painter);

    for(int i = 0; i < rect.size(); i++)
        if(rect[i].getInt() <= 1)
            rect[i].draw(&painter);
}

MainWindow::~MainWindow()
{
    delete ui;
}

